# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-pa
pkgver=5.23.2
pkgrel=0
pkgdesc="Plasma applet for audio volume management using PulseAudio"
# armhf blocked by qt5-qtdeclarative
# s390x, mips64 and riscv64 blocked by polkit -> plasma-workspace
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only OR LGPL-3.0-only AND GPL-2.0-only"
depends="pulseaudio kirigami2"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	kglobalaccel-dev
	knotifications-dev
	ki18n-dev
	plasma-workspace-dev
	pulseaudio-dev
	libcanberra-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-pa-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUSE_GCONF=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
22a8430df55e73580c45c637e2ac86c94cfac068d3106af26ce5cc668db193bdea79421176fb156312068300e7c32cdf95ba00c38fd584228475f2ec93f4e36d  plasma-pa-5.23.2.tar.xz
"
