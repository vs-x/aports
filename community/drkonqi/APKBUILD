# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=drkonqi
pkgver=5.23.2
pkgrel=0
pkgdesc="The KDE crash handler"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kwallet-dev
	kwidgetsaddons-dev
	kxmlrpcclient-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	syntax-highlighting-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/drkonqi-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=OFF # Broken
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ecbc40e1529fc89ceb8f7383651fc23a17d80f847a8003c7148fa7b1d29d3ae88d249576b6fb7cafd6641cce35dea699140773876d1dd81b0e2a6cfbfe6682e9  drkonqi-5.23.2.tar.xz
"
